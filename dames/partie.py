__authors__ = 'Alexandre Poulin et Mélanie Raynauld'

from dames.damier import Damier
from dames.position import Position
from dames.exceptions import ErreurPositionCible, ErreurPositionSource, PieceInexistante


class Partie:
    """Gestionnaire de partie de dames. Ne contient pas la logique de jeu: celle-ci doit être faite dans l'interface
    graphique associée. Dans un projet où une interface en console aurait aussi été disponible, nous aurions créé
    un autre module contenant l'interaction avec l'utilisateur + la logique de jeu.

    Attributes:
        damier (Damier): Le damier de la partie, contenant notamment les pièces.
        couleur_joueur_courant (str): Le joueur à qui c'est le tour de jouer.
        doit_prendre (bool): Un booléen représentant si le joueur actif doit absolument effectuer une prise
            de pièce. Sera utile pour valider les mouvements et pour gérer les prises multiples.
        position_source_selectionnee (Position): La position source qui a été sélectionnée. Utile pour sauvegarder
            cette information avant de poursuivre. Contient None si aucune pièce n'est sélectionnée.
        position_source_forcee (Position): Une position avec laquelle le joueur actif doit absolument jouer. Le
            seul moment où cette position est utilisée est après une prise: si le joueur peut encore prendre
            d'autres pièces adverses, il doit absolument le faire. Ce membre contient None si aucune position n'est
            forcée.

    """
    def __init__(self):
        """Constructeur de la classe Partie. Initialise les attributs à leur valeur par défaut. Le damier est construit
        avec les pièces à leur valeur initiales, le joueur actif est le joueur blanc, et celui-ci n'est pas forcé
        de prendre une pièce adverse. Aucune position source n'est sélectionnée, et aucune position source n'est forcée.

        """
        self.damier = Damier()
        self.couleur_joueur_courant = "blanc"
        self.doit_prendre = False
        self.position_source_selectionnee = None
        self.position_source_forcee = None

    def assurer_position_source_valide(self, position_source):
        """Vérifie la validité de la position source, notamment:
            - Est-ce que la position contient une pièce?
            - Est-ce que cette pièce est de la couleur du joueur actif?
            - Si le joueur doit absolument continuer son mouvement avec une prise supplémentaire, a-t-il choisi la
              bonne pièce?

        Cette méthode ne retourne rien, par contre elle lancera une exception si la position source n'est pas valide.

        Warning:
            Utilisez les attributs de la classe pour connaître les informations sur le jeu! (le damier, le joueur
            actif, si une position source est forcée, etc.

        Warning:
            Vous avez accès ici à un attribut de type Damier. vous avez accès à plusieurs méthodes pratiques
            dans le damier qui vous simplifieront la tâche ici :)

        Args:
            position_source (Position): La position source à valider.

        Raises:
            ErreurPositionSource: Si la position source n'est pas valide.

        """
        try:
            piece_source = self.damier.recuperer_piece_a_position(position_source)

        except PieceInexistante:
            raise ErreurPositionSource("Position source invalide: aucune pièce à cet endroit")

        if not piece_source.couleur == self.couleur_joueur_courant:
            raise ErreurPositionSource("Position source invalide: pièce de mauvaise couleur")

        if self.position_source_forcee is not None:
            if not (self.position_source_forcee == position_source):
                raise ErreurPositionSource("Position source invalide: vous devez faire jouer avec la pièce en ({},{})".format(self.position_source_forcee.ligne, self.position_source_forcee.colonne))

    def assurer_position_cible_valide(self, position_cible):
        """Vérifie si la position cible est valide (en fonction de la position source sélectionnée). Doit non seulement
        vérifier si le déplacement serait valide (utilisez les méthodes que vous avez programmées dans le Damier!), mais
        également si le joueur a respecté la contrainte de prise obligatoire.

        Raises:
            ErreurPositionCible: Si la position cible n'est pas valide.

        """
        deplacement_valide = self.damier.piece_peut_se_deplacer_vers(self.position_source_selectionnee, position_cible)
        saut_valide = self.damier.piece_peut_sauter_vers(self.position_source_selectionnee, position_cible)

        if self.doit_prendre:
            if not saut_valide:
                raise ErreurPositionCible("Le déplacement demandé n'est pas une prise alors qu'une prise est possible")

        if not (deplacement_valide or saut_valide):
            raise ErreurPositionCible("Position cible invalide")

    def sauvegarder(self, nom_fichier):
        """Sauvegarde une partie dans un fichier. Le fichier contiendra:
        - Une ligne indiquant la couleur du joueur courant.
        - Une ligne contenant True ou False, si le joueur courant doit absolument effectuer une prise à son tour.
        - Une ligne contenant None si self.position_source_forcee est à None, et la position ligne,colonne autrement.
        - Le reste des lignes correspondent au damier. Voir la méthode convertir_en_chaine du damier pour le format.

        Warning:
            Lorsqu'on écrit ou lit dans un fichier texte, il faut s'assurer de bien convertir les variables
            dans le bon type.

        Exemple de contenu de fichier :

        blanc
        True
        6,1
        1,2,noir,dame
        1,6,blanc,pion
        4,1,noir,dame
        5,2,noir,pion
        6,1,blanc,dame


        Args:
            nom_fichier (str): Le nom du fichier où sauvegarder.

        """
        with open(nom_fichier, "w") as f:
            f.write("{}\n".format(self.couleur_joueur_courant))
            f.write("{}\n".format(self.doit_prendre))
            if self.position_source_forcee is not None:
                f.write("{},{}\n".format(self.position_source_forcee.ligne, self.position_source_forcee.colonne))
            else:
                f.write("None\n")
            f.writelines(self.damier.convertir_en_chaine())

    def charger(self, nom_fichier):
        """Charge une partie à partir d'un fichier. Le fichier a le même format que la méthode de sauvegarde.

        Warning: N'oubliez pas de bien convertir les chaînes de caractères lues!

        Args:
            nom_fichier (str): Le nom du fichier à lire.

        """
        with open(nom_fichier) as f:
            self.couleur_joueur_courant = f.readline().rstrip("\n")
            doit_prendre_string = f.readline().rstrip("\n")
            if doit_prendre_string == "True":
                self.doit_prendre = True
            else:
                self.doit_prendre = False

            position_string = f.readline().rstrip("\n")
            if position_string == "None":
                self.position_source_forcee = None
            else:
                ligne_string, colonne_string = position_string.split(",")
                self.position_source_forcee = Position(int(ligne_string), int(colonne_string))

            self.damier.charger_dune_chaine(f.read())