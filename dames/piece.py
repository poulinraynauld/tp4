__authors__ = 'Alexandre Poulin et Mélanie Raynauld'


class Piece:
    """Une pièce d'un jeu de dames.

    Attributes:
        couleur (str): La couleur de la pièce. Les deux valeurs possibles sont 'blanc' et 'noir'.
        type_de_piece (str): Le type de pièce. Les deux valeurs possibles sont 'pion' et 'dame'.

    """
    def __init__(self, couleur, type_de_piece):
        """Constructeur de la classe Piece. Initialise les deux attributs de la classe.

        Args:
            couleur (str): La couleur de la pièce ('blanc' ou 'noir').
            type_de_piece (str) : Le type de pièce ('pion' ou 'dame').

        """
        if couleur not in {'blanc', 'noir'}:
            raise ValueError("Couleur de pièce invalide. Doit être 'blanc' ou 'noir'.")

        if type_de_piece not in {'pion', 'dame'}:
            raise ValueError("Type de pièce invalide. Doit être 'pion' ou 'dame'.")

        self.couleur = couleur
        self.type_de_piece = type_de_piece

    def est_pion(self):
        """Détermine si la pièce est un pion.

        Returns:
            (bool) : True si la pièce est un pion, False autrement.

        """
        return self.type_de_piece == "pion"

    def est_dame(self):
        """Détermine si la pièce est une dame.

        Returns:
            (bool) : True si la pièce est une dame, False autrement.

        """
        return self.type_de_piece == "dame"

    def est_blanche(self):
        """Détermine si la pièce est de couleur blanche.

        Returns:
            (bool) : True si la pièce est blanche, False autrement.

        """
        return self.couleur == "blanc"

    def est_noire(self):
        """Détermine si la pièce est de couleur noire.

        Returns:
            (bool) : True si la pièce est noire, False autrement.

        """
        return self.couleur == "noir"

    def promouvoir(self):
        """Cette méthode permet de promouvoir une pièce, c'est à dire la transformer en dame.

        """
        self.type_de_piece = "dame"

    def __repr__(self):
        """Méthode spéciale indiquant à Python comment représenter une instance de Piece sous la forme d'une chaîne
        de caractères. Permet notamment d'afficher une pièce à l'écran.

        """
        #if self.est_blanche() and self.est_pion():
        #    return "o"
        #elif self.est_blanche() and self.est_dame():
        #    return "O"
        #elif self.est_noire() and self.est_pion():
        #    return "x"
        #else:
        #    return "X"

        #Si vous avez la police d'écriture Deja Vu:
        if self.est_blanche() and self.est_pion():
            return "\u26C0"
        elif self.est_blanche() and self.est_dame():
            return "\u26C1"
        elif self.est_noire() and self.est_pion():
            return "\u26C2"
        else:
            return "\u26C3"