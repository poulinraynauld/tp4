__authors__ = 'Alexandre Poulin et Mélanie Raynauld'

from dames.piece import Piece
from dames.position import Position
from dames.exceptions import PieceInexistante, ErreurDeplacement

class Damier:
    """Plateau de jeu d'un jeu de dames. Contient un ensemble de pièces positionnées à une certaine position
    sur le plateau.

    Attributes:
        cases (dict): Dictionnaire dont une clé représente une Position, et une valeur correspond à la Piece
            positionnée à cet endroit sur le plateau.

        n_lignes (int): Le nombre de lignes du plateau.
        n_colonnes (int): Le nombre de colonnes du plateau.
        n_rangées (int): Le nombre de rangées de pièces pour chaque joueur.

    """
    def __init__(self, n_lignes=8, n_colonnes=8, n_rangees=3):
        """Constructeur du Damier. Initialise un damier initial de 8 lignes par 8 colonnes.

        Args:
            n_lignes (int): Le nombre de lignes du damier (défaut: 8, minimum: 4)
            n_colonnes (int): Le nombre de colonnes du damier (défaut: 8, minimum: 4).
            n_rangees (int): Le nombre de rangées de pièces pour chaque joueur (défaut: 3, minimum: 1).

        Raises:
            ValueError: Si le nombre de lignes, de colonnes ou de rangées est invalide.
        """
        if n_lignes < 4:
            raise ValueError("Le nombre de lignes doit être plus grand ou égal à 4.")
        if n_colonnes < 4:
            raise ValueError("Le nombre de colonnes doit être plus grand ou égal à 4.")
        if n_rangees < 1:
            raise ValueError("Le nombre de rangées doit être plus grand ou égal à 1.")
        if n_lignes - 2 * n_rangees < 1:
            raise ValueError("Le nombre de lignes est insuffisant pour le nombre de rangées choisi.")

        self.cases = {}
        self.n_lignes = n_lignes
        self.n_colonnes = n_colonnes
        self.n_rangees = n_rangees
        self.initialiser_damier()

    def initialiser_damier(self):
        """Initialise un damier avec les pièces positionnées à leur endroit par défaut. Dépend du nombre de lignes,
        colonnes et rangées.

        """
        # Noirs
        for ligne in range(0, self.n_rangees):
            for colonne in range(0 + (ligne + 1) % 2, self.n_colonnes, 2):
                self.cases[Position(ligne, colonne)] = Piece("noir", "pion")

        # Blancs
        for ligne in range(self.n_lignes - 1, self.n_lignes - 1 - self.n_rangees, -1):
            for colonne in range(0 + (ligne + 1) % 2, self.n_colonnes, 2):
                self.cases[Position(ligne, colonne)] = Piece("blanc", "pion")

    def recuperer_piece_a_position(self, position):
        """Récupère une pièce dans le damier à partir d'une position.

        Args:
            position (Position): La position où récupérer la pièce.

        Returns:
            Piece: La pièce à la position reçue en argument.

        Raises:
            PieceInexistante: Si aucune pièce n'est disponible à cette position.

        """
        try:
            return self.cases[position]

        except KeyError:
            raise PieceInexistante("La pièce en position {} n'existe pas.".format(position))

    def piece_presente(self, position):
        """Vérifie si un pièce est présente ou non à une certaine position.

        Args:
            position (Position): La position où vérifier.

        Returns:
            bool: True si une pièce est présente, False autrement.

        """
        try:
            piece = self.recuperer_piece_a_position(position)
            return True

        except PieceInexistante:
            return False

    def position_est_dans_damier(self, position):
        """Vérifie si les coordonnées d'une position sont dans les bornes du damier (entre 0 inclusivement et le nombre
        de lignes/colonnes, exclusement.

        Args:
            position (Position): La position à valider.

        Returns:
            bool: True si la position est dans les bornes, False autrement.

        """
        return 0 <= position.ligne < self.n_lignes and 0 <= position.colonne < self.n_colonnes

    def piece_peut_se_deplacer_vers(self, position_piece, position_cible):
        """Cette méthode détermine si une pièce (à la position reçue) peut se déplacer à une certaine position cible.
        On parle ici d'un déplacement standard (et non une prise).

        Une pièce doit être positionnée à la position_piece reçue en argument (retourner False autrement).

        Une pièce de type pion ne peut qu'avancer en diagonale (vers le haut pour une pièce blanche, vers le bas pour
        une pièce noire). Une pièce de type dame peut avancer sur n'importe quelle diagonale, peu importe sa couleur.
        Une pièce ne peut pas se déplacer sur une case déjà occupée par une autre pièce. Une pièce ne peut pas se
        déplacer à l'extérieur du damier.

        Args:
            position_piece (Position): La position de la pièce source du déplacement.
            position_cible (Position): La position cible du déplacement.

        Returns:
            bool: True si la pièce peut se déplacer à la position cible, False autrement.

        """
        try:
            piece = self.recuperer_piece_a_position(position_piece)

        except PieceInexistante:
            return False

        if not self.position_est_dans_damier(position_cible):
            return False

        if self.piece_presente(position_cible):
            return False

        if position_cible not in position_piece.quatre_positions_diagonales():
            return False

        if piece.est_blanche() and not piece.est_dame() and position_cible \
                not in position_piece.positions_diagonales_haut():
            return False

        if piece.est_noire() and not piece.est_dame() and position_cible \
                not in position_piece.positions_diagonales_bas():
            return False

        return True

    def piece_peut_sauter_vers(self, position_piece, position_cible):
        """Cette méthode détermine si une pièce (à la position reçue) peut sauter vers une certaine position cible.
        On parle ici d'un déplacement qui "mange" une pièce adverse.

        Une pièce doit être positionnée à la position_piece reçue en argument (retourner False autrement).

        Une pièce ne peut que sauter de deux cases en diagonale. N'importe quel type de pièce (pion ou dame) peut sauter
        vers l'avant ou vers l'arrière. Une pièce ne peut pas sauter vers une case qui est déjà occupée par une autre
        pièce. Une pièce ne peut faire un saut que si elle saute par dessus une pièce de couleur adverse.

        Args:
            position_piece (Position): La position de la pièce source du saut.
            position_cible (Position): La position cible du saut.

        Returns:
            bool: True si la pièce peut sauter vers la position cible, False autrement.

        """
        try:
            piece = self.recuperer_piece_a_position(position_piece)

        except PieceInexistante:
            return False

        if not self.position_est_dans_damier(position_cible):
            return False

        if self.piece_presente(position_cible):
            return False

        if position_cible not in position_piece.quatre_positions_sauts():
            return False

        position_mediane = Position((position_piece.ligne + position_cible.ligne) // 2,
                                    (position_piece.colonne + position_cible.colonne) // 2)

        try:
            piece_a_manger = self.recuperer_piece_a_position(position_mediane)

        except PieceInexistante:
            return False

        if piece.couleur == piece_a_manger.couleur:
            return False

        return True

    def piece_peut_se_deplacer(self, position_piece):
        """Vérifie si une pièce à une certaine position a la possibilité de se déplacer (sans faire de saut).

        Warning:
            N'oubliez pas qu'étant donné une position, il existe une méthode dans la classe Position retournant
            les positions des quatre déplacements possibles.

        Args:
            position_piece (Position): La position source.

        Returns:
            bool: True si une pièce est à la position reçue et celle-ci peut se déplacer, False autrement.

        """
        for position_saut in position_piece.quatre_positions_diagonales():
            if self.piece_peut_se_deplacer_vers(position_piece, position_saut):
                return True

        return False

    def piece_peut_faire_une_prise(self, position_piece):
        """Vérifie si une pièce à une certaine position a la possibilité de faire une prise.

        Warning:
            N'oubliez pas qu'étant donné une position, il existe une méthode dans la classe Position retournant
            les positions des quatre sauts possibles.

        Args:
            position_piece (Position): La position source.

        Returns:
            bool: True si une pièce est à la position reçue et celle-ci peut faire une prise. False autrement.

        """
        for position_saut in position_piece.quatre_positions_sauts():
            if self.piece_peut_sauter_vers(position_piece, position_saut):
                return True

        return False

    def piece_de_couleur_peut_se_deplacer(self, couleur):
        """Vérifie si n'importe quelle pièce d'une certaine couleur reçue en argument a la possibilité de se déplacer
        vers une case adjacente (sans saut).

        Warning:
            Réutilisez les méthodes déjà programmées!

        Args:
            couleur (str): La couleur à vérifier.

        Returns:
            bool: True si une pièce de la couleur reçue peut faire un déplacement standard, False autrement.

        """
        for position, piece in self.cases.items():
            if piece.couleur == couleur:
                if self.piece_peut_se_deplacer(position):
                    return True

        return False

    def piece_de_couleur_peut_faire_une_prise(self, couleur):
        """Vérifie si n'importe quelle pièce d'une certaine couleur reçue en argument a la possibilité de faire un
        saut, c'est à dire vérifie s'il existe une pièce d'une certaine couleur qui a la possibilité de prendre une
        pièce adverse.

        Warning:
            Réutilisez les méthodes déjà programmées!

        Args:
            couleur (str): La couleur à vérifier.

        Returns:
            bool: True si une pièce de la couleur reçue peut faire un saut (une prise), False autrement.

        """
        for position, piece in self.cases.items():
            if piece.couleur == couleur:
                if self.piece_peut_faire_une_prise(position):
                    return True

        return False

    def deplacer(self, position_source, position_cible):
        """Effectue le déplacement sur le damier. Si le déplacement est valide, on doit mettre à jour le dictionnaire
        self.cases, en déplaçant la pièce à sa nouvelle position (et possiblement en supprimant une pièce adverse qui a
        été prise).

        Cette méthode doit également promouvoir un pion en dame si celui-ci atteint l'autre extrémité du plateau.

        Warning:
            Ne dupliquez pas de code! Vous avez déjà programmé (ou allez programmer) des méthodes permettant
            de valider si une pièce peut se déplacer vers un certain endroit ou non.

        Args:
            position_source (Position): La position source du déplacement.
            position_cible (Position): La position cible du déplacement.

        Raises:
            ErreurDeplacement: Si le déplacement est invalide.

        """
        try:
            deplacement_valide = self.piece_peut_se_deplacer_vers(position_source, position_cible)
            saut_valide = self.piece_peut_sauter_vers(position_source, position_cible)

            if deplacement_valide or saut_valide:
                piece = self.recuperer_piece_a_position(position_source)
                self.cases[position_cible] = piece
                del self.cases[position_source]
            else:
                raise ErreurDeplacement("Déplacement invalide.")

            if saut_valide:
                position_prise = Position((position_cible.ligne + position_source.ligne) // 2,
                                          (position_cible.colonne + position_source.colonne) // 2)
                del self.cases[position_prise]

        except PieceInexistante:
            raise ErreurDeplacement("Déplacement invalide.")

        # Si la pièce est rendue au bout, elle est promue reine!
        if (position_cible.ligne == 0 and piece.est_blanche()) or (position_cible.ligne == self.n_lignes - 1 and piece.est_noire()):
            piece.promouvoir()

    def convertir_en_chaine(self):
        """Retourne une chaîne de caractères où chaque case est écrite sur une ligne distincte.
        Chaque ligne contient l'information suivante (respectez l'ordre et la manière de séparer les éléments):
        ligne,colonne,couleur,type

        Par exemple, un damier à deux pièces (un pion noir en (1, 2) et une dame blanche en (6, 1)) serait représenté
        par la chaîne suivante:

        1,2,noir,pion
        6,1,blanc,dame

        Cette méthode pourrait par la suite être réutilisée pour sauvegarder un damier dans un fichier.

        Returns:
            (str): La chaîne de caractères construite.

        """
        chaine = ""
        for position, piece in self.cases.items():
            chaine += "{},{},{},{}\n".format(position.ligne, position.colonne, piece.couleur, piece.type_de_piece)

        return chaine

    def charger_dune_chaine(self, chaine):
        """Vide le damier actuel et le remplit avec les informations reçues dans une chaîne de caractères. Le format de
        la chaîne est expliqué dans la documentation de la méthode convertir_en_chaine.

        Args:
            chaine (str): La chaîne de caractères.

        """
        self.cases.clear()
        for information_piece in chaine.split("\n"):
            if information_piece != "":
                ligne_string, colonne_string, couleur, type_piece = information_piece.split(",")
                self.cases[Position(int(ligne_string), int(colonne_string))] = Piece(couleur, type_piece)

    def __repr__(self):
        """Cette méthode spéciale permet de modifier le comportement d'une instance de la classe Damier pour
        l'affichage. Faire un print(un_damier) affichera le damier à l'écran.

        """
        s = " {}\n".format(("  {} " * self.n_colonnes).format(*range(self.n_colonnes)))
        s += " {}+\n".format("+---" * self.n_colonnes)
        for i in range(0, self.n_lignes):
            s += str(i)+"| "
            for j in range(0, self.n_colonnes):
                if Position(i, j) in self.cases:
                    s += str(self.cases[Position(i, j)])+" | "
                else:
                    s += "  | "
            s += "\n {}+\n".format("+---" * self.n_colonnes)

        return s
