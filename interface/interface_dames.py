__authors__ = 'Alexandre Poulin et Mélanie Raynauld'

from tkinter import Tk, Canvas, Label, NSEW, Button, LabelFrame, Frame, E, S, W, N, EW, messagebox, filedialog, \
    CENTER, Text, WORD, Scrollbar, VERTICAL, DISABLED
from dames.damier import Damier
from dames.position import Position
from dames.piece import Piece
from dames.partie import Partie
from dames.exceptions import ErreurPositionCible, ErreurPositionSource, PieceInexistante, ErreurDeplacement, \
    ErreurSauvegarde, ErreurChargement

# Variable globale
#global liste_position, numero_mouvement
liste_position = str("Je suis une chaîne de caractère \n j'attend de trouver un moyen \n pour que je sois remplie.")
#numero_mouvement = 1

class CanvasDamier(Canvas):
    def __init__(self, parent, n_pixels_par_case, le_damier):
        # Nombre de lignes et de colonnes.
        self.n_lignes = 8
        self.n_colonnes = 8
        self.damier = le_damier

        # Nombre de pixels par case, variable.
        self.n_pixels_par_case = n_pixels_par_case

        # Liste des positions de chaque case du damier
        self.liste_position_case = []

        # Appel du constructeur de la classe de base (Canvas).
        super().__init__(parent, width=self.n_lignes * n_pixels_par_case,
                         height=self.n_colonnes * self.n_pixels_par_case)

        # Dictionnaire contenant les pièces.
        #self.damier = self.partie.damier.cases

        # On fait en sorte que le redimensionnement du canvas redimensionne son contenu. Cet événement étant également
        # généré lors de la création de la fenêtre, nous n'avons pas à dessiner les cases et les pièces dans le
        # constructeur.
        self.bind('<Configure>', self.redimensionner)


    def dessiner_cases(self):
        for i in range(self.n_lignes):
            for j in range(self.n_colonnes):
                debut_ligne = i * self.n_pixels_par_case
                fin_ligne = debut_ligne + self.n_pixels_par_case
                debut_colonne = j * self.n_pixels_par_case
                fin_colonne = debut_colonne + self.n_pixels_par_case
                self.liste_position_case.append(Position(i, j))

                # On détermine la couleur.
                if (i + j) % 2 == 0:
                    couleur = 'white'
                else:
                    couleur = 'gray'

                # On dessine le rectangle. On utilise l'attribut "tags" pour être en mesure de récupérer les éléments
                # par la suite.
                self.create_rectangle(debut_colonne, debut_ligne, fin_colonne, fin_ligne, fill=couleur, tags='case')
        #print(self.liste_position_case)
        #print(self.liste_position_case[2], type(self.liste_position_case[2]))


    def dessiner_pieces(self):
        # Caractères unicode représentant les pièces. Vous avez besoin de la police d'écriture Deja-Vu, qui est
        # installée automatiquement si vous installez le logiciel LibreOffice.

        # Pour tout paire position, pièce:
        for position, piece in self.damier.cases.items():
            #print(type(position), type(piece))
            #print(piece, type(piece))
            #print(position)
            #print("test position.ligne", position.ligne, type(position.ligne))
            #print("test position.colonne", position.colonne, type(position.colonne))

            # On dessine la pièce dans le canvas, au centre de la case. On utilise l'attribut "tags" pour être en
            # mesure de récupérer les éléments dans le canvas.
            coordonnee_y = position.ligne * self.n_pixels_par_case + self.n_pixels_par_case // 2
            coordonnee_x = position.colonne * self.n_pixels_par_case + self.n_pixels_par_case // 2
            self.create_text(coordonnee_x, coordonnee_y, text=str(piece),
                             font=('Deja Vu', self.n_pixels_par_case // 2), tags='piece')


    def redimensionner(self, event):
        # Nous recevons dans le "event" la nouvelle dimension dans les attributs width et height. On veut un damier
        # carré, alors on ne conserve que la plus petite de ces deux valeurs.
        nouvelle_taille = min(event.width, event.height)

        # Calcul de la nouvelle dimension des cases.
        self.n_pixels_par_case = nouvelle_taille // self.n_lignes

        # On supprime les anciennes cases et on ajoute les nouvelles.
        self.delete('case')
        self.dessiner_cases()

        # On supprime les anciennes pièces et on ajoute les nouvelles.
        self.delete('piece')
        self.dessiner_pieces()

    def changer_couleur_piece_selectionnee(self, position):
        """ Change la couleur de la pièce sélectionnée.

        Args:
            position (Position):

        """




    def changer_couleur_case(self, position, couleur):
        """ Change la couleur d'une case

        Args:
            position (Position): La position de la case
        """

        # On change la couleur de la case du damier pour montrer que la pièce est bien sélectionnée
        print(position, type(position))
        print(position.ligne)
        debut_ligne = position.ligne * self.n_pixels_par_case
        fin_ligne = debut_ligne + self.n_pixels_par_case
        debut_colonne = position.colonne * self.n_pixels_par_case
        fin_colonne = debut_colonne + self.n_pixels_par_case
        self.create_rectangle(debut_colonne, debut_ligne, fin_colonne, fin_ligne, fill=couleur)
        coordonnee_y = position.ligne * self.n_pixels_par_case + self.n_pixels_par_case // 2
        coordonnee_x = position.colonne * self.n_pixels_par_case + self.n_pixels_par_case // 2
        self.create_text(coordonnee_x, coordonnee_y, text=self.damier[position], font=('Deja Vu',
                                                                                       self.n_pixels_par_case // 2))


    def mise_a_jour_damier(self):
        """ Rafraichit le damier

        :return:
        """
        # On supprime les anciennes cases et on ajoute les nouvelles.
        self.delete('case')
        self.dessiner_cases()

        # On supprime les anciennes pièces et on ajoute les nouvelles.
        self.delete('piece')
        self.dessiner_pieces()



class Fenetre(Tk):
    def __init__(self):
        super().__init__()

        # Nom de la fenêtre.
        self.title("Damier")
        # La position sélectionnée.
        #self.position_selectionnee = self.partie.position_source_selectionnee

        # Truc pour le redimensionnement automatique des éléments de la fenêtre.
        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(0, weight=1)

        # Création d'un objet partie
        self.partie = Partie()

        #Création de l'attibut position cible sélectionnee
        self.position_cible_selectionnee = None

        # Création du canvas damier.
        self.canvas_damier = CanvasDamier(self, 40, self.partie.damier)
        self.canvas_damier.grid(sticky=NSEW, padx=10, pady=10)

        # Ajout d'une étiquette d'information.
        self.messages = Label(self)
        self.messages.grid()

        # Ajout d'une étiquette des messages d'erreur.
        self.messages_erreur = Label(self)
        self.messages_erreur.grid(row=5)

        #Cadre principal (droite du canevas)
        cadre_principal = LabelFrame(self, width=40, padx=10, pady=10)
        cadre_principal.grid(row=0, rowspan=5, column=2, columnspan=2, sticky=NSEW, padx=10, pady=10)

        #Cadre pour les boutons
        cadre_boutons = LabelFrame(cadre_principal, text="Boutons")
        cadre_boutons.grid(row=0, column=2, sticky=NSEW, padx=10, pady=10)

        # Ajout d'un cadre pour l'étiquette d'information au joueur.
        self.cadre_messages_joueur = LabelFrame(cadre_principal, text="Message au joueur", width=40, padx=10, pady=10,
                                                bg="pink")
        self.cadre_messages_joueur.grid(row=4, rowspan=2, columnspan=3, sticky=EW + S, padx=10, pady=10)

        # Ajout d'une étiquette d'information au joueur.
        self.messages_joueur = Label(self.cadre_messages_joueur, text="Tour du joueur {}!".
                                     format(self.partie.couleur_joueur_courant), font="normal", padx=10, pady=10,
                                     bg="pink")
        self.messages_joueur.grid(row=4, rowspan=2, columnspan=3, sticky=E + W + S, padx=10, pady=10)

        #Création des boutons
        Button(cadre_boutons, text="Règles du jeu", command=self.reglement).grid(sticky=E + W)
        Button(cadre_boutons, text="Nouvelle partie", command=self.nouvelle_partie).grid(sticky=E + W)
        Button(cadre_boutons, text="Sauvegarder", command=self.sauvegarder_une_partie).grid(sticky=E + W)
        Button(cadre_boutons, text="Charger une partie", command=self.charger_une_partie).grid(sticky=E + W)
        Button(cadre_boutons, text="Options", command=self.options).grid(sticky=E + W)
        Button(cadre_boutons, text="Quitter", command=self.quit).grid(sticky=E + W)

        #Cadre pour la zone de texte présentant les déplacements
        cadre_deplacement = LabelFrame(cadre_principal, text="Historique des déplacements")
        cadre_deplacement.grid(row=0, column=0, sticky=EW + N, padx=10, pady=10)

        #Création de la zone de texte: Historique des déplacements
        self.historique = Text(cadre_deplacement, height=5, width=20, wrap=WORD)
        self.historique = Label(cadre_deplacement, text="\n {} \n".format(liste_position))
        self.historique.grid(row=0, column=0, sticky=NSEW)
        self.historique.config(state=DISABLED)


        #Création de la scrollbar
        barre_defilement = Scrollbar(self, orient=VERTICAL)

        #Association du déplacement de la scrollbar avec la position visible dans le wigdet text
        #barre_defilement.config(command=self.historique_deplacement.yview)
        #self.historique.config(yscrollcommand=barre_defilement.set)
        #barre_defilement.grid(row=5, column=1, sticky=NSEW)


        # On lie un clic sur le CanvasDamier à une méthode.
        self.canvas_damier.bind('<Button-1>', self.selectionner)
        #self.canvas_damier.bind('<Button-3>', self.drag)
        self.canvas_damier.bind('<B1-Motion>', self.drag)

        # Si la sélection est valide on change la case de couleur pour indiquer qu'elle est bien sélectionnée
        #print(self.canvas_damier.partie.assurer_position_source_valide(self.position_selectionnee))
        #if self.canvas_damier.partie.assurer_position_source_valide(self.position_selectionnee):
        #self.canvas_damier.changer_couleur_case(self.position_selectionnee,'cyan')
        #else:
        #    self.canvas_damier.changer_couleur_case(self.position_selectionnee,'gray')


    def selectionner(self, event):
        # On trouve le numéro de ligne/colonne en divisant les positions en y/x par le nombre de pixels par case.
        ligne = event.y // self.canvas_damier.n_pixels_par_case
        colonne = event.x // self.canvas_damier.n_pixels_par_case

        self.messages_erreur.configure(text="")

        #On vérifie si le joueur doit prendre une pièce adverse
        if self.partie.damier.piece_de_couleur_peut_faire_une_prise(self.partie.couleur_joueur_courant):
            self.partie.doit_prendre = True
            self.messages_joueur.configure(text="Le joueur {} doit faire une prise!".
                                           format(self.partie.couleur_joueur_courant))
        else:
            self.partie.doit_prendre = False
            self.messages_joueur.configure(text="Le joueur {} ne peut pas faire de prise!".
                                           format(self.partie.couleur_joueur_courant))

        # Validation de la position source
        try:
            self.partie.assurer_position_source_valide(Position(ligne, colonne))
            # La position source est valide donc on change la valeur de l'attribut position_selectionnee.
            self.partie.position_source_selectionnee = Position(ligne, colonne)
            print("La position source est valide!")
            piece = self.canvas_damier.damier.cases[self.partie.position_source_selectionnee]
            self.messages['foreground'] = 'black'
            self.messages['text'] = 'Pièce sélectionnée : {} à la position {}.'.\
                format(piece, self.partie.position_source_selectionnee)

        except ErreurPositionSource as e:
            print(e)
            self.messages['foreground'] = 'red'
            self.messages['text'] = "Erreur", e

            print("La position source n'est pas valide!")

        except PieceInexistante as e:
            self.messages['foreground'] = 'red'
            self.messages['text'] = e

        except:
            self.messages['foreground'] = 'red'
            self.messages['text'] = "Erreur inconnue!!!"



            #for self.position_selectionnee in self.canvas_damier.liste_position_case:
            #    self.canvas_damier.changer_couleur_case(self.position_selectionnee,'cyan')

            # On change la couleur de la case du damier pour montrer que la pièce est bien sélectionnée
            #if self.partie.position_source_selectionnee is not None:
            #    self.canvas_damier.changer_couleur_case(self.partie.position_source_selectionnee,'cyan')
            #else:
            #    self.canvas_damier.changer_couleur_case(self.partie.position_source_selectionnee,'gray')
            #debut_ligne = ligne * self.canvas_damier.n_pixels_par_case
            #fin_ligne = debut_ligne + self.canvas_damier.n_pixels_par_case
            #debut_colonne = colonne * self.canvas_damier.n_pixels_par_case
            #fin_colonne = debut_colonne + self.canvas_damier.n_pixels_par_case
            #self.canvas_damier.create_rectangle(debut_colonne, debut_ligne, fin_colonne, fin_ligne, fill='cyan')
            #coordonnee_y = ligne * self.canvas_damier.n_pixels_par_case + self.canvas_damier.n_pixels_par_case // 2
            #coordonnee_x = colonne * self.canvas_damier.n_pixels_par_case + self.canvas_damier.n_pixels_par_case // 2
            #self.canvas_damier.create_text(coordonnee_x, coordonnee_y, text=piece,
            # font=('Deja Vu', self.canvas_damier.n_pixels_par_case//2))


    def drag(self, event):
        """ Gestion de l'événement bouton gauche enfoncé """
        ligne = event.y // self.canvas_damier.n_pixels_par_case
        colonne = event.x // self.canvas_damier.n_pixels_par_case

        self.messages_erreur.configure(text="")

        self.position_cible_selectionnee = Position(ligne, colonne)
        print("La position source est: {} et la position cible est {}".format(self.partie.position_source_selectionnee,
                                                                              self.position_cible_selectionnee))
        print(type(self.position_cible_selectionnee))
        #print("nouveau damier:", self.canvas_damier.damier)

        try:
            self.partie.assurer_position_cible_valide(self.position_cible_selectionnee)
            print("La position cible est valide!")
            self.partie.damier.deplacer(self.partie.position_source_selectionnee, self.position_cible_selectionnee)
            print("La pièce a été déplacé à la position cible!")
            self.canvas_damier.mise_a_jour_damier()

            if self.partie.doit_prendre == True:
                if self.partie.damier.piece_de_couleur_peut_faire_une_prise(self.partie.couleur_joueur_courant):
                    self.partie.doit_prendre = True
                    self.messages_joueur.configure(text="Le joueur {} doit faire une autre prise!".
                                               format(self.partie.couleur_joueur_courant))
                    self.partie.damier.deplacer(self.partie.position_source_selectionnee, self.position_cible_selectionnee)

            #else:
            #    self.partie.doit_prendre = False
            #    self.messages_joueur.configure(text="Le joueur {} ne peut pas faire de prise!".
            #                                   format(self.partie.couleur_joueur_courant))

            #Le tour du joueur actif est terminé, on change de joueur
            if self.partie.couleur_joueur_courant == "blanc":
                self.partie.couleur_joueur_courant = "noir"
            else:
                self.partie.couleur_joueur_courant = "blanc"

            #Affiche le message au joueur
            self.messages_joueur = Label(self.cadre_messages_joueur, text="Tour du joueur {}!".
                                         format(self.partie.couleur_joueur_courant), font="normal", padx=10, pady=10,
                                         bg="pink")
            self.messages_joueur.grid(row=4, rowspan=2, columnspan=3, sticky=E + W + S, padx=10, pady=10)
            # Historique des déplacements
            self.historique_deplacement()

        except ErreurDeplacement as e:
            print(e, type(e))
            self.messages['foreground'] = 'red'
            self.messages['text'] = "Erreur: ", e

        except ErreurPositionCible as e:
            print(e, type(e))
            self.messages['foreground'] = 'red'
            self.messages['text'] = "Erreur: ", e

        #if self.partie.assurer_position_cible_valide(self.position_cible_selectionnee):


    def nouvelle_partie(self):
        """ Initialiser un nouveau damier

        """

        messagebox.askokcancel("Confirmaton nouvelle partie", "Voulez-vous vraiment recommencer une nouvelle partie?")

        #TODO Fonction à compléter... Il y a un problème ici!
        # Problème: Quand on fait Nouvelle partie on ouvre une nouvelle fenêtre et il faut ensuite cliquer 2 fois sur
        # le bouton quitter!

        #On remet les variables à leur valeur initiale
        self.partie.couleur_joueur_courant = "blanc"
        self.partie.doit_prendre = False
        self.partie.position_source_forcee = None
        self.partie.position_source_forcee = None


        self.destroy()
        #self.quit
        self = Fenetre()
        self.mainloop()
        self.partie = Partie()
        #self.partie.damier.initialiser_damier()
        #print(self.partie.damier.cases)
        self.canvas_damier.mise_a_jour_damier()

#self.partie = Partie()
        #self.partie.damier.initialiser_damier()
        #self.canvas_damier = CanvasDamier(self, 40, self.partie.damier.cases)
        #self.canvas_damier.mise_a_jour_damier()
        #self.canvas_damier.grid(row = 1, sticky=NSEW)


    def sauvegarder_une_partie(self):
        """ Ouvre une boîte de message pour enregistrer une partie.

        """
        try:
            nom_fichier = filedialog.asksaveasfilename(title="Sauvegarder la partie")
            if nom_fichier == "":
                raise ErreurSauvegarde("Erreur de sauvegarde")
            else:
                self.partie.sauvegarder(nom_fichier)
        except ErreurSauvegarde as e:
            messagebox.showerror(e, "Erreur!\nVous devez entrer un nom de fichier!")

    def charger_une_partie(self):
        """ Ouvre une boîte de message pour ouvrir une partie sauvegardée.

        """
        try:
            nom_fichier = filedialog.askopenfilename(title="Sauvegarder la partie")
            if nom_fichier == "":
                raise ErreurChargement("Erreur de chargement")
            else:
                self.partie.charger(nom_fichier)
                #print(self.partie.doit_prendre)
                #print(self.partie.position_source_forcee)
                #print(self.partie.damier.cases)
                self.canvas_damier.mise_a_jour_damier()
        except ErreurChargement as e:
            messagebox.showerror(e, "Erreur!\nVous devez sélectionner un fichier!")

    def reglement(self):
        """ Affiche les règles du jeu de dame dans une fenêtre.

        """

        regle_du_jeu = """
        Le joueur avec les pièces blanches commence en premier.
        Le pion se déplace en diagonal vers l'avant seulement(vers le haut
        pour les blancs, vers le bas pour les noirs). Une case doit être
        libre pour pouvoir s'y déplacer.

        Le pion qui arrive à l'autre extrémité du damier devient une dame.
        La dame se déplace en diagonal vers l'avant et vers l'arrière
        (vers le haut et vers le bas).

        Une pièce fait une prise en sautant par dessus une pièce adverse.
        Si une pièce peut faire une prise, il doit la faire. Un pion
        mange vers l'avant, tandis qu'une dame peut manger vers l'avant
        et vers l'arrière.

        Le premier joueur a mangé toute les pièces adverses gagne la
        partie."""

        messagebox.showinfo("Règlement du jeu de dame", regle_du_jeu)

    def historique_deplacement(self):
        """ Donne la liste de l'historique des déplacements réalisés.
        Return:
            List: La liste des déplacements réalisés.
        """
        #global numero_mouvement, liste_position
       # x = self.partie.position_source_selectionnee.colonne
        #y = self.partie.position_source_selectionnee.ligne
       # w = self.position_cible_selectionnee.colonne
        #z = self.position_cible_selectionnee.ligne

        #liste_position.filetypes +=(x,y), (w,z)
        #historique_position += position
        #return historique_position
        pass
    def options(self):
        """ Affiche une fenêtre qui permet de modifier d'autres options (ex, couleur du damier, ...)

        """
        fenetre_options = Tk()
        etiquette = Label(fenetre_options, text="Veuillez sélectionner l'option\nque vous voulez modifier.",
                          padx=10, pady=10)
        etiquette.grid()

        Button(fenetre_options, text="Changer le thème", command=self.changer_theme).grid(sticky=EW)
        Button(fenetre_options, text="Fermer", command=fenetre_options.destroy).grid(sticky=EW)

        fenetre_options.mainloop



    def changer_theme(self):
        """ Changer la couleur du fond du damier.

        """
        pass