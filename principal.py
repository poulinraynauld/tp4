__authors__ = 'Alexandre Poulin et Mélanie Raynauld'

from interface.interface_dames import Fenetre
if __name__ == '__main__':
    # Point d'entrée principal du TP4. Vous n'avez pas à modifier ce fichier, il ne sert qu'à exécuter votre programme.
    # Le fichier à éditer est interface/interface_dames.py.

    f = Fenetre()
    f.mainloop()